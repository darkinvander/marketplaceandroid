package com.yasir.marketplace.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.yasir.marketplace.R
import com.yasir.marketplace.databinding.ActivityLoginBinding
import com.yasir.marketplace.util.SharedPref

class LoginActivity : AppCompatActivity() {
    private var _binding : ActivityLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val spref = SharedPref(this)
        if(spref.getIsLogin()){
            binding.tvStatus.text = "SUDAH LOGIN"
        }else binding.tvStatus.text = "BELUM LOGIN"

        binding.btnLogin.setOnClickListener {
            spref.setIsLogin(true)
            onBackPressed()
        }

        binding.btnLogout.setOnClickListener {
            spref.setIsLogin(false)
            onBackPressed()
        }
    }
}